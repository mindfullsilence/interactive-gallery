////////////////////////////////////////////////////////////////////////////////
/**
 * InteractiveGallery.js
 * Author: Timothy Anderson
 *
 * Instantiation:
 * $('container').interactiveGallery();
 * or
 * var el = document.querySelector('container');
 * InteractiveGallery.apply(el, [el, {}]);
 *
 * Options:
 * {
 *  classes: {
 *    active: 'active',
 *    previous: 'previous',
 *    next: 'next',
 *    before: 'before',
 *    after: 'after'
 *  },
 *  infinite: true,
 *  aspectRatio: 9 / 16
 * }
 *
 * If "infinite" is true, the slider will loop endlessley in either direction.
 *
 */
////////////////////////////////////////////////////////////////////////////////




var $ = require('jquery');
var jQuery = window.$ = window.jQuery = window.jQuery || $;

// constructor
var InteractiveGallery = function(element, properties) {
    var self      = this;

    // instance related
    self.element  = element;
    self.$element = $(element);
    self.props    = $.extend({}, InteractiveGallery.DEFAULTS, properties);

    self.resources = {
        assets: {
            slides  : {
                container : self.element.querySelector( '.slides' ),
                slideItems: [].slice.call( self.element.querySelectorAll( '.slider-item' ) ),
                active    : self.element.querySelector( '.slider-item' )
            },
            dots    : {
                container : self.element.querySelector( '.slider-dots' ),
                get dots() {return [].slice.call( self.element.querySelectorAll( '.slider-dot' ) )},
                active    : self.element.querySelector( '.slider-dot' ),
                clicked   : self.element.querySelector( '.slider-dot' )
            },
            captions: {
                container   : self.element.querySelector( '.captions' ),
                captionItems: [].slice.call( self.element.querySelectorAll( '.caption-item' ) ),
                active      : self.element.querySelector( '.caption-item' )
            },
            controls: {
                prev     : self.element.querySelector( '.slider-bottom .prev' ),
                next     : self.element.querySelector( '.slider-bottom .next' )
            }
        }
    };

    self._bindEvents();
    // get 'er done
    self._init();
};

// 'static' constants
InteractiveGallery.SELECTOR = '.interactive-gallery';
InteractiveGallery.DEFAULTS = {
    classes: {
        active: 'active',
        previous: 'previous',
        next: 'next',
        before: 'before',
        after: 'after'
    },
    infinite: true,
    aspectRatio: 9 / 16
};
InteractiveGallery.INSTANCES = [];

InteractiveGallery.prototype._init = function() {
    var self = this;
    var dataTrigger = function() {
        var el = this;
        self.$element.trigger(
          'click' +
          $(el).data('interactiveGallery') +
          '.interactiveGallery.zion'
        );
    };

    self.touchstartx = undefined;
    self.touchmovex = undefined;
    self.slideWidth = self.resources.assets.slides.container.clientWidth;

    if(!self.resources.assets.slides.active) {
        self.resources.assets.slides.active = self.resources.assets.slides.slideItems[0];
    }
    if(!self.resources.assets.captions.active) {
        self.resources.assets.captions.active = self.resources.assets.captions.captionItems[0];
    }
    $(self.resources.assets.controls.prev ).attr('data-interactive-gallery', 'prev');
    $(self.resources.assets.controls.next ).attr('data-interactive-gallery', 'next');
    self.$element.on('click.interactiveGallery', '[data-interactive-gallery]', function(e) {
        dataTrigger.apply( e.currentTarget);
        $('.tooltip-trigger').tooltip('hide');
    });

    self.$element.on('click.interactiveGallery', '.slider-dot', function(e) {
        $(self.resources.assets.dots.dots ).removeClass(self.props.classes.active);
        $(e.currentTarget ).addClass(self.props.classes.active);
        self.resources.assets.dots.clicked = e.currentTarget;
        self.resources.assets.dots.active = e.currentTarget;
        self.$element.trigger('clickdot.interactiveGallery.zion');
    });

    $(self.resources.assets.slides.active ).addClass(self.props.classes.active);
    $(self.resources.assets.captions.active ).addClass(self.props.classes.active);
    $(self.resources.assets.dots.active ).addClass(self.props.classes.active);
    self._dupeSlides();
    self.setSiblingClasses();
    self.setActiveCaption();
    self.setActiveDot();

    self.$element.trigger('init.interactiveGallery.zion');
};

InteractiveGallery.prototype._bindEvents = function() {
    var self  = this;

    /**
     * Manual reset functionality. This is useful when dynamically adding slides
     * to the gallery. Call via the following:
     * $('.interactive-gallery:first').data('interactiveGallery.zion').reset();
     * Or reset all of them:
     * $(InteractiveGallery.INSTANCES).each(function(i, item) {
     *      InteractiveGallery.INSTANCES[i].reset();
     * }
     */
    self.$element.on('reset.interactiveGallery.zion', function() {
        self._init();
    });

    self.$element.on('init.interactiveGallery.zion', function() {
        self.resize();
    });
    self.$element.on('resize.interactiveGallery.zion', function() {
       self.resize();
    });
    self.$element.on('clicknext.interactiveGallery.zion', function() {
        self.nextImage();
    });
    self.$element.on('clickprev.interactiveGallery.zion', function() {
        self.prevImage();
    });
    self.$element.on('beforeSwitchImage.interactiveGallery.zion', function() {
        self.setActiveCaption();
        self.setActiveDot();
    });
    self.$element.on('clickdot.interactiveGallery.zion', function() {
       self.clickDot();
    });
    self.$element.on('nextImage.interactiveGallery.zion', function() {
       self.moveFirstToLast();
    });
    self.$element.on('prevImage.interactiveGallery.zion', function() {
       self.moveLastToFirst();
    });
    self.$element.on('afterSwap.interactiveGallery.zion', function() {
       self._setResources();
    });
    self.$element.on('afterSwitchImage.interactiveGallery.zion', function() {
       self.setSiblingClasses();
    });
};

InteractiveGallery.prototype._setResources = function() {
    var self = this;
    self.resources.assets.slides.slideItems = [].slice.call( self.element.querySelectorAll( '.slider-item' ) );
    self.resources.assets.captions.captionItems = [].slice.call( self.element.querySelectorAll( '.caption-item' ) );
};

InteractiveGallery.prototype._dupeSlides = function() {
    var self = this,
      dupPre = $(self.resources.assets.slides.slideItems ).clone(),
      dupPost = $(self.resources.assets.slides.slideItems ).clone(),
        originals = $(self.resources.assets.slides );
    originals.attr('data-original', '');
    self.resources.assets.slides.originals = originals;
    dupPre.add(dupPost)
      .removeClass(
          self.props.classes.active + ' ' +
          self.props.classes.previous + ' ' +
          self.props.classes.next
        )
      .attr('data-duplication', '' );
    dupPre.prependTo(self.resources.assets.slides.container);
    dupPost.appendTo(self.resources.assets.slides.container);
    self.resources.assets.slides.duplicates = [].slice.call(self.element.querySelectorAll('[data-duplication]'));
    self._setResources();
};

InteractiveGallery.prototype.reset = function() {
    var self = this;
    $(self.resources.assets.slides.duplicates ).remove();
    delete self.resources.assets.slides.duplicates;
    self._setResources();
    $(self.resources.assets.slides.slideItems ).removeClass(
      self.props.classes.active + ' ' +
      self.props.classes.previous + ' ' +
      self.props.classes.next + ' ' +
      self.props.classes.before + ' ' +
      self.props.classes.after
    );
    $(self.resources.assets.slides.active ).addClass(self.props.classes.active);
    $(self.resources.assets.captions.active ).addClass(self.props.classes.active);

    self.resources = {
        assets: {
            slides  : {
                container : self.element.querySelector( '.slides' ),
                slideItems: [].slice.call( self.element.querySelectorAll( '.slider-item' ) ),
                active    : self.element.querySelector( '.slider-item.active' )
            },
            captions: {
                container   : self.element.querySelector( '.captions' ),
                captionItems: [].slice.call( self.element.querySelectorAll( '.caption-item' ) ),
                active      : self.element.querySelector( '.caption-item.active' )
            },
            controls: {
                prev     : self.element.querySelector( '.slider-bottom .prev' ),
                next     : self.element.querySelector( '.slider-bottom .next' )
            }
        }
    };
    self.setSiblingClasses();
    self.$element.off('click.interactiveGallery');
    self.$element.trigger('reset.interactiveGallery.zion');
};

InteractiveGallery.prototype.resize = function() {
    var self = this,
        container = self.resources.assets.slides.container,
        width = container.offsetWidth;
    if(self.props.aspectRatio != false) {
        $(container).css('height', width * self.props.aspectRatio);
    }
    else if(self.props.height != false) {
        $(container).css('height', self.props.height);
    }
    $(window ).add('html, body').one('width.resizeEnd.zion', function() {
        self.$element.trigger('resize.interactiveGallery.zion');
    });
    self.$element.trigger('resized.interactiveGallery.zion');
};

InteractiveGallery.prototype.nextImage = function() {
    var self = this;

    $(self.resources.assets.slides.active).removeClass(self.props.classes.active);
    self.resources.assets.slides.active =
        $(self.resources.assets.slides.active ).next().length ?
        $(self.resources.assets.slides.active ).next()[0] :
        self.resources.assets.slides.slideItems[0];
    self.$element.trigger('beforeNextImage.interactiveGallery.zion');
    self.$element.trigger('beforeSwitchImage.interactiveGallery.zion');
    $(self.resources.assets.slides.active).addClass(self.props.classes.active);
    self.$element.trigger('nextImage.interactiveGallery.zion');
    self.$element.trigger('afterSwitchImage.interactiveGallery.zion');

};

InteractiveGallery.prototype.prevImage = function() {
    var self       = this;
    var $active = $(self.resources.assets.slides.active);
    $active.removeClass(self.props.classes.active);
    self.resources.assets.slides.active =
        $active.prev().length ?
        $active.prev() :
        self.resources.assets.slides.slideItems[self.resources.assets.slides.slideItems.length-1];
    self.$element.trigger('beforePrevImage.interactiveGallery.zion');
    self.$element.trigger('beforeSwitchImage.interactiveGallery.zion');
    $(self.resources.assets.slides.active).addClass(self.props.classes.active);
    self.$element.trigger('prevImage.interactiveGallery.zion');
    self.$element.trigger('afterSwitchImage.interactiveGallery.zion');
};

InteractiveGallery.prototype.setActiveCaption = function() {
    var self = this,
    $activeSlide = $(self.resources.assets.slides.active ),
    captionId = $activeSlide.data('captionId' ),
    blankCaption = self.resources.assets.captions.container.querySelector('[data-caption-id="blank"]' ),
    activate = function() {
        $(self.resources.assets.captions.active ).removeClass(self.props.classes.active);
        self.resources.assets.captions.active = this;
        self.$element.trigger('beforeSetCaption.interactiveGallery.zion');
        $(self.resources.assets.captions.active ).addClass(self.props.classes.active);
        self.$element.trigger('setCaption.interactiveGallery.zion');
    },
    caption;
    if(captionId === 'blank') {
        activate.apply(blankCaption);
    }
    else if(typeof captionId === 'number') {
        caption = self.resources.assets.captions.container.querySelector('[data-caption-id="'+ captionId + '"]');
        if(caption !== self.resources.assets.captions.active) {
            activate.apply(caption);
        }
    }
};

InteractiveGallery.prototype.setActiveDot = function() {
    var self = this,
        $activeSlide = $(self.resources.assets.slides.active ),
        dot = $activeSlide.data('dot' ),
        activate = function() {
            $(self.resources.assets.dots.active ).removeClass(self.props.classes.active);
            self.resources.assets.dots.active = this;
            self.$element.trigger('beforeSetDot.interactiveGallery.zion');
            $(self.resources.assets.dots.active ).addClass(self.props.classes.active);
            self.$element.trigger('setDot.interactiveGallery.zion');
        },
        thedot;
    if(typeof dot === 'number') {
        thedot = self.resources.assets.dots.container.querySelector('[data-dot="'+dot+'"]');
        if(thedot !== self.resources.assets.dots.active) {
            activate.apply(thedot);
        }
    }
};

InteractiveGallery.prototype.moveFirstToLast = function() {
    var self = this,
        slideItems = self.resources.assets.slides.slideItems,
        first = slideItems.shift();
    if(self.props.infinite) {
        self.$element.trigger('beforeSwap.interactiveGallery.zion');
        $(first).appendTo(self.resources.assets.slides.container);
        self.$element.trigger('afterSwap.interactiveGallery.zion');
    }
};

InteractiveGallery.prototype.moveLastToFirst = function() {
    var self = this,
        slideItems = self.resources.assets.slides.slideItems,
        last = slideItems.pop();
    if(self.props.infinite) {
        self.$element.trigger('beforeSwap.interactiveGallery.zion');
        $(last).prependTo(self.resources.assets.slides.container);
        self.$element.trigger('afterSwap.interactiveGallery.zion');
    }
};

InteractiveGallery.prototype.setSiblingClasses = function() {
    var self = this,
        activeSlide = self.resources.assets.slides.active,
        previous = $(activeSlide ).prev()[0] || self.resources.assets.slides.slideItems[self.resources.assets.slides.slideItems.length - 1 ],
        next = $(activeSlide ).next()[0] || self.resources.assets.slides.slideItems[0];
    $(self.resources.assets.slides.slideItems ).removeClass(
      self.props.classes.previous + ' ' +
      self.props.classes.next + ' ' +
      self.props.classes.before + ' ' +
      self.props.classes.after
    );
    self.$element.trigger('beforeSiblingClasses.interactiveGallery.zion');
    $(previous ).prevAll().addClass(self.props.classes.before);
    $(previous ).addClass(self.props.classes.previous);
    $(next ).addClass(self.props.classes.next);
    $(next ).nextAll().addClass(self.props.classes.after);
    self.$element.trigger('siblingClasses.interactiveGallery.zion');
};

InteractiveGallery.prototype.clickDot = function() {
    var self = this;
    var goal = $(self.resources.assets.dots.clicked ).data('dot');
    var current = $(self.resources.assets.slides.active ).data('dot');
    if(current === goal) {
        return;
    }
    else {
        self.$element.one('afterSwitchImage.interactiveGallery.zion', function() {
            self.clickDot();
        });
    }
    if(current < goal) {
        self.$element.trigger('clicknext.interactiveGallery.zion');
    }
    else {
        self.$element.trigger('clickprev.interactiveGallery.zion');
    }
};

module.exports = InteractiveGallery;
