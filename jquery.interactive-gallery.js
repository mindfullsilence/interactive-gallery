/**
 * User: TimAnderson
 * Date: 7/21/16
 * Time: 3:48 PM
 */
var jQuery = $ = window.$ = window.jQuery = window.jQuery || require('jquery');
var InteractiveGallery = require('./interactive-gallery.js');

var old;

// Integrate With jQuery
// ---------------------------------------------------------------------------
// instantiate
function Plugin(option) {
    return this.each(function () {
        var $this       = $(this);
        var data        = $this.data('zion.interactiveGallery');
        var options     = $.extend({}, InteractiveGallery.DEFAULTS, $this.data(), typeof option == 'object' && option);
        var interactiveGallery   = new InteractiveGallery(this, options);
        InteractiveGallery.INSTANCES.push(interactiveGallery);
        //if(false === $this.is(InteractiveGallery.SELECTOR)) {
        //    $this.addClass(InteractiveGallery.SELECTOR.substr(1));
        //}
        if (!data) {
            $this.data('zion.interactiveGallery', (data = interactiveGallery));
        }
        if (typeof option == 'string') data[option]();
    });
}

// make jQuery plugin
old                                 = $.fn.interactiveGallery;
$.fn.interactiveGallery             = Plugin;
$.fn.interactiveGallery.Constructor = InteractiveGallery;
$.fn.interactiveGallery.noConflict  = function () {
    $.fn.interactiveGallery = old;
    return this;
};

// pif
module.exports = InteractiveGallery;
